import React from 'react';
import AppNavigator from './src/navigation/AppNavigator';
// import { StyleSheet, Text, View } from 'react-native';
import {createAppContainer} from 'react-navigation';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {ReduxNetworkProvider} from 'react-native-offline';
import {persistor, store} from './store';
import '@react-native-community/netinfo';
import {API_URL} from 'react-native-dotenv';

const prefix = 'polyteach://';

const AppContainer = createAppContainer(AppNavigator);

//const Loading = () => <div>Loading...</div>;

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate /*loading={<Loading/>}*/ persistor={persistor}>
          <ReduxNetworkProvider
            pingTimeout={10000}
            pingServerUrl={API_URL + '/ping'}
            shouldPing={true}
            pingInterval={30000}
            pingOnlyIfOffline={false}
            pingInBackground={false}
            httpMethod={'HEAD'}>
            <AppContainer uriPrefix={prefix} />
          </ReduxNetworkProvider>
        </PersistGate>
      </Provider>
    );
  }
}
